//
//  ViewController.swift
//  AutoLayoutAndConstraints
//
//  Created by Louise Chan on 2020-11-05.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    
    private var labelTopConstraint: NSLayoutConstraint!
    private var labelBottomConstraint: NSLayoutConstraint!
    private var labelLeftConstraint: NSLayoutConstraint!
    private var labelRightConstraint: NSLayoutConstraint!
    private var labelCenterHorizConstraint: NSLayoutConstraint!
    private var labelCenterVertConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Initially setup constraints for center position
        setupLabelEdgeConstraintNames()
        clearAllLabelConstraints()
        labelCenterHorizConstraint.isActive = true
        labelCenterVertConstraint.isActive = true
        segmentedControl.selectedSegmentIndex = 2
        
    }
    
    
    private func setupLabelEdgeConstraintNames() {
        labelLeftConstraint = nameLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor)

        labelRightConstraint = nameLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
        
        labelTopConstraint = nameLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor)

        labelBottomConstraint = nameLabel.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        
        
        labelCenterHorizConstraint = nameLabel.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor)
        
        labelCenterVertConstraint = nameLabel.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor)
    }
    
    private func clearAllLabelConstraints() {
        labelLeftConstraint?.isActive = false
        labelTopConstraint?.isActive = false
        labelCenterVertConstraint?.isActive = false
        labelCenterHorizConstraint?.isActive = false
        labelBottomConstraint?.isActive = false
        labelRightConstraint?.isActive = false
    }

    @IBAction func segmentedControlValueChanged(_ sender: UISegmentedControl) {
        clearAllLabelConstraints()
        switch sender.selectedSegmentIndex {
        case 0:
            print("Left was selected")
            labelCenterVertConstraint.isActive = true
            labelLeftConstraint.isActive = true
            nameLabel.textAlignment = .left
        case 1:
            print("Right was selected")
            labelCenterVertConstraint.isActive = true
            labelRightConstraint.isActive = true
            nameLabel.textAlignment = .right

        case 2:
            print("Center was selected")
            labelCenterVertConstraint.isActive = true
            labelCenterHorizConstraint.isActive = true
            nameLabel.textAlignment = .center
        case 3:
            print("Top was selected")
            labelCenterHorizConstraint.isActive = true
            labelTopConstraint.isActive = true
            nameLabel.textAlignment = .center
        case 4:
            print("Bottom was selected")
            labelCenterHorizConstraint.isActive = true
            labelBottomConstraint.isActive = true
            nameLabel.textAlignment = .center
        default:
            print("invalid selection")
        }
    }
    
    
    
}

